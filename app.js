const app = Vue.createApp({
    data(){
        return {
            friendslist:[
                { 'id': 1,'name': 'faruk','phone': '01728926770','email':'f@gmalil.com'},
                { 'id': 2,'name': 'omar','phone': '01949499569','email':'o@gmalil.com'},
            ],
        };
    },
    
});
app.component('friend-contact',{
    template:`<li>
          <h2>{{friend.name}}</h2>
          <button @click="detailsactive"> {{detailsareavailable ? 'Hide' : 'Show'}} Details</button>
          <ul v-if="detailsareavailable">
            <li><strong>Phone:</strong> {{friend.phone}}</li>
            <li><strong>Email:</strong> {{friend.email}}</li>
          </ul>
        </li>`,
    data(){
        return { detailsareavailable:false,
        friend:{ 'id': 1,'name': 'faruk','phone': '01728926770','email':'f@gmalil.com'} };
    },
    methods:{
        detailsactive(){
            this.detailsareavailable = !this.detailsareavailable;
        },
    },
});
app.mount('#app');